# E10Angular

Este projeto tem a finalidade de alterar o portifólio pessoal [e10](https://elessandroprestes.github.io/e10/) para [Angular CLI](https://github.com/angular/angular-cli) version 8.2.1.

Podendo ser acessado no link ao lado - [e10-angular](https://elessandroprestes.github.io/e10-angular/)

## Construído com 

* [ANGULAR](https://angular.io/) - Plataforma de aplicações web e front-end baseado em typescript...
* [SASS](https://sass-lang.com/) - Linguagem de extensão CSS
* [TYPESCRIPT](https://www.typescriptlang.org/) - Superconjunto de Javascript, desenvolvido pela Microsoft..
* [NODE JS](https://nodejs.org/en/) - Interpretador de código Javascript
* [NPM](https://www.npmjs.com/) - Gerenciador de pacotes

## Autor

* **Elessandro Prestes Macedo** - [linkedin](https://www.linkedin.com/in/elessandro-prestes-macedo-278189126/)




